-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-08-2020 a las 22:22:28
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd_taxi_maps`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `basetaxi`
--

CREATE TABLE `basetaxi` (
  `idBase` int(11) NOT NULL,
  `Nombre` varchar(20) NOT NULL,
  `Descripcion` varchar(60) NOT NULL,
  `Ubicacion` varchar(60) NOT NULL,
  `Numero_T` int(11) NOT NULL,
  `id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `basetaxi`
--

INSERT INTO `basetaxi` (`idBase`, `Nombre`, `Descripcion`, `Ubicacion`, `Numero_T`, `id`) VALUES
(1, 'Sitio Juarez', 'Zona centro de Ixmiquilpan, frente al bar Malecon', 'Felipe Angeles 46, Centro, 423', 7575, NULL),
(2, 'Sitio Morelos', 'Zona centro de Ixmiquilpan, a un costado del mercado Morelos', 'Álvaro Obregón, San Antonio, 42302 Ixmiquilpan, Hgo.', 45231, NULL),
(3, 'Sitio San Antonio', 'Zona centro de Ixmiquilpan', 'Plazuela San Antonio S/N, San Antonio, 42302 Ixmiquilpan, Hg', 5468, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `basetaxi`
--
ALTER TABLE `basetaxi`
  ADD PRIMARY KEY (`idBase`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `basetaxi`
--
ALTER TABLE `basetaxi`
  MODIFY `idBase` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `basetaxi`
--
ALTER TABLE `basetaxi`
  ADD CONSTRAINT `basetaxi_ibfk_1` FOREIGN KEY (`id`) REFERENCES `usuarios` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
