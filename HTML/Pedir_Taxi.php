<?php
        session_start();
        $usuario=$_SESSION['usuario'];
        $numControlA=$usuario;
        $conexion=mysqli_connect('localhost','root','','bd_taxi_maps');
            include('conexion.php');
            $query="SELECT id,Nombre,ApPaterno,Usuario FROM usuarios WHERE Usuario='$numControlA'" ;

            $res = $conexion->query($query);
            while ($row=$res->fetch_assoc()) {
                error_reporting(0);
                $idUsuario=$row['id'];
                $NombreUsuario=$row['Nombre'];
                $ApPaterno=$row['ApPaterno'];       
                $numControlA=$row['usuario'];
            }     
        ?>
<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=yes">
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Sitios de Taxis</title>
    <link rel="shortcut icon" href="../Imagenes/logo.ico" />
    <link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/Pedir_Taxi.css" /> 
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-core.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-service.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>
  </head>
  <body id="markers-on-the-map">
      <header class="header">
            <div class="container logo-nav-container">
                <a href="#" class="logo"><img src="../imagenes/logo.jpeg" width="100" height="50"></a>
                <nav class="navegacion">
                    <ul>
                        <li><a href="quienes_somos.php">Bienvenida</a></li>
                        <li><a href="contactos_confianza.php">Contacto de Confianzo</a></li>
                        <li><a href="Pedir_Taxi.php">Pedir Taxi</a></li>
                        <li><a href="Info_sitio.php">Información del sitio</a></li>
                        <li><a href="info_propietario.php">Información del Propietario</a></li>
                        <li><a href="../index.php">Cerrar sesión</a></li>
                    </ul>
                </nav>
            </div> 
        </header>
        <main class="main">
            <div class="container"></div>
            <div class="container_2">
                    <section class="portafolio">
                <div class="page-header">
                    <h4>Sitios de Taxis</h4>
                    <p>Bienvenid@: <?php echo $NombreUsuario.' '.$ApPaterno ?></p>
                    <p>Numero de identificación: <?php echo $idUsuario?></p>
                </div>
                <center>
                    <div id="map"></div>
                </center>
                    </section>
            </div>
        </main>
        <footer class="footer">
            <div class="container">
                <p>Copy RIGHT</p>
                <p>Creado por:
                Jesus Salas Morgado, David Ángel Hernández Gutiérrez, Diana Itzel Mejía Mejía, Osccar Lara Patricio, Adair Cruz Gerrero</p>
            </div>  
        </footer>
    <script>
        //Step 1: initialize communication with the platform
        // In your own code, replace variable window.apikey with your own apikey
        var platform = new H.service.Platform({
          'apikey': 'zMj59usgyTLAUqfSOnhhWJtt4Ym7r5GMP1BJMkeNPUo'
        });
        var defaultLayers = platform.createDefaultLayers();

        //Step 2: initialize a map - this map is centered over Europe
        var map = new H.Map(document.getElementById('map'),
          defaultLayers.vector.normal.map,{
          center: {lat:20.4831, lng:-99.2171},
          zoom: 14,
          pixelRatio: window.devicePixelRatio || 1
        });
        //Linea de codigo que hace que aparescan los botones de acercar y alaejar.
        var ui = H.ui.UI.createDefault(map, defaultLayers);
        //Linea de codigo que permite mover el mapa.
        var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
        
        function clickTomark(){
            var marker=new H.map.Marker({lat:20.486773, lng:-99.215653});
               map.addObject(marker);
            map.addEventListener('tap', function(evt){
               var bubble=new H.ui.InfoBubble({lat:20.486773, lng:-99.215653}, {
                    content:'<b><p>Sitio:San Antonio</p> <p>Ubicacion:Plazuela San Antonio S/N, San Antonio, 42302 Ixmiquilpan, Hgo</p> <p>Numero Telefonico:5468<p><li><a href="Info_sitio.php">Para mas imformacion presione Aqui</a></li></b>' 
                 });
                 ui.addBubble(bubble);
            });
        }
        clickTomark();
    </script>
  </body>
</html>