<?php
    session_start();
    $usuario=$_SESSION['usuario'];
    $numControlA=$usuario;
    $conexion=mysqli_connect('localhost','root','','bd_taxi_maps');
            include('conexion.php');
            $query="SELECT id,Nombre,ApPaterno,Usuario FROM usuarios WHERE Usuario='$numControlA'" ;

            $res = $conexion->query($query);
            while ($row=$res->fetch_assoc()) {
                error_reporting(0);
                $idUsuario=$row['id'];
                $NombreUsuario=$row['Nombre'];
                $ApPaterno=$row['ApPaterno'];       
                $numControlA=$row['usuario'];
            }     
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Taxi-Maps</title>
        <link rel="shortcut icon" href="../Imagenes/logo.ico" />
        <link rel="stylesheet" type="text/css" href="../css/quienes_somos.css">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <header class="header">
            <div class="container logo-nav-container">
                <a href="#" class="logo"><img src="../imagenes/logo.jpeg" width="100" height="50"></a>
                <nav class="navegacion">
                    <ul>
                        <li><a href="quienes_somos.php">Bienvenida</a></li>
                        <li><a href="contactos_confianza.php">Contacto de Confianza</a></li>
                        <li><a href="Pedir_Taxi.php">Pedir Taxi</a></li>
                        <li><a href="Info_sitio.php">Informaciòn del sitio</a></li>
                        <li><a href="info_propietario.php">Información del Propietario</a></li>
                        <li><a href="../index.php">Cerrar sesión</a></li>
                    </ul>
                </nav>
            </div> 
        </header>
        
        <main class="main">
            <div class="container"></div>
            <div class="container_2">
                    <section class="portafolio">
                    <h4>Taxi-Maps</h4>
                    <p>Bienvenid@: <?php echo $NombreUsuario.' '.$ApPaterno ?></p>
                    <p>Numero de identificación: <?php echo $idUsuario?></p>
                    <center><img src="../imagenes/quienes somos.jpg" alt="" class="portafolio-img"></center>
                    <p>
                        En el municipio de Ixmiquilpan el servicio de taxis no cuenta con un soporte óptimo como ninguna tecnología de ubicación para sus usuarios, puesto que no existe un medio de comunicación entre taxistas y sus clientes potenciales que proporcione información verídica de la ubicación y contacto de los distintos sitios que ofrecen el servicio de transporte en taxi, lo que genera pérdidas económicas para los taxistas y contratiempos para los clientes interesados en utilizar el servicio eso sin mencionar el nivel de inseguridad que hoy en día se suscita en el municipio, generando desconfianza por parte de las personas, por lo que se busca que exista un medio de comunicación confiable y seguro que garantice un buen servició y la integridad de las personas. 
                    </p>
                    <p>
                        El desarrollo del sitio web con base al sistema de mapas y navegación de HERE TECHNOLOGIES, pretende permitir a usuarios locales y extranjeros el servicio de taxis en la zona de Ixmiquilpan, ubicar un sitio de taxi cercano, contar con información de contacto con el respectivo sitio de taxis, información sobre que taxi y los datos del taxista, y así como la posibilidad de que un usuario al registrarse en la aplicación esta enviara información al momento sobre el traslado de un usuario a contactos de su agrados por el mismo al momento de usar el servicio con la finalidad de para garantizar la seguridad del usuario.
                    </p>
                    </section>
            </div>
        </main>
        <footer class="footer">
            <div class="container">
                <p>Copy RIGHT</p>
                <p>Creado por: Jesus Salas Morgado, David Ángel Hernández Gutiérrez, Diana Itzel Mejía Mejía, Osccar Lara Patricio, Adair Cruz Gerrero</p>
            </div>  
        </footer>
    </body>
</html>